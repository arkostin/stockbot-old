#
# Testing QUANDL without pandas_datareader
#

import matplotlib.pyplot as plt
import pandas_datareader.data as web
import pandas

data = pandas.read_json("https://api.iextrading.com/1.0/stock/aapl/chart/date/20181120")
data.average.plot()
plt.show()
