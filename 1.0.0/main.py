# Alexander Kostin
#
# main.py
# Main file for stockbot

import pandas_datareader.data as web
import pandas as pd
import numpy as np
from talib import RSI, BBANDS
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import json

# Starting program
print "Starting stockbot..."

# Setting up variables
symbol = 'AAPL'
date = '20190114'

# Get market data
price = pd.read_json("https://api.iextrading.com/1.0/stock/" + symbol + "/chart/date/" + date)

# Drop all the null values
price = price.dropna()

# Get closing values, dates, rsi, and bollinger bands
dates = price['minute'].values

# Function to get integer from a "%H:%m" format string
def time_to_int(foo):
    return int(foo[:2]) * 60 + int(foo[3:]) - 569

# Format dates
dates = dates.tolist()
for i in range(len(dates)):
    dates[i]=time_to_int(dates[i])

# Get the 5, 8, and 13-bar moving averages for a stock.
def get_5_8_13_moving_averages(dates_arr, averages_arr):
    ma5 = [-1] * len(dates_arr)
    ma8, ma13 = list(ma5), list(ma5)
    for i in dates[4:]:
        ma5[i-1] = (sum(averages_arr[i-5:i]))/5
        ma8[i-1] = (sum(averages_arr[i-8:i]))/8
        ma13[i-1] = (sum(averages_arr[i-13:i]))/13

    return ma5[5:], ma8[8:], ma13[13:]

# Uses candlestick patterns and plots everything
def candlestick_plot():
    
    # Set up variables
    candlestick_width = 0.35

    # Get all the values necessary for candlesticks
    close_val = price['close'].values
    open_val = price['open'].values
    high_val = price['high'].values
    low_val = price['low'].values
    average_val = price['average'].values

    ma5, ma8, ma13 = get_5_8_13_moving_averages(dates, average_val)
    
    print "Starting to plot candlesticks"
    # Plot the candlesticks
    for i in range(len(dates)):

        # Choose color for candlestick and plot shadows
        c = ''
        shadow_c = '#000000'
        if close_val[i] < open_val[i]:
            c = "#ff0000"
            plt.plot([dates[i]] * 2, [close_val[i], low_val[i]], color=shadow_c)
            plt.plot([dates[i]] * 2, [open_val[i], high_val[i]], color=shadow_c)
        else:
            c = "#00ff00"
            plt.plot([dates[i]] * 2, [close_val[i], high_val[i]], color=shadow_c)
            plt.plot([dates[i]] * 2, [open_val[i], low_val[i]], color=shadow_c)

        # Draw body
        plt.fill_between(x=[dates[i]-candlestick_width,dates[i]+candlestick_width],y1=open_val[i],y2=close_val[i],color=c)
        plt.gca().add_patch(Rectangle(xy=[dates[i]-candlestick_width,open_val[i]], width=candlestick_width*2, height=close_val[i]-open_val[i], fill=False))

    # Plot moving averages
    plt.plot(dates[5:], ma5, color="blue")
    plt.plot(dates[8:], ma8, color="green")
    plt.plot(dates[13:], ma13, color="orange")

    # Do the actual buying/selling simulation

    # Initialize variables
    capital = 500.00;
    long_shares = 0;
    short_shares = 0;

    print "Starting with $", capital

    # Buy and sell based on moving averages only
    for i in range(len(dates))[13:]:
        if ma13[i-13] < ma5[i-5] and long_shares == 0:
            long_shares += int(capital / close_val[i])
            capital -= long_shares * close_val[i]
            plt.scatter(dates[i], close_val[i], c="g")
            #print "At", i, "buying", long_shares, "at", close_val[i]
        elif ma13[i-13] >= ma5[i-5] and long_shares > 0:
            #print ma13[i], ma5[i]
            capital += long_shares * close_val[i]
            #print "At", i, "selling", long_shares, "at", close_val[dates[i]]
            long_shares = 0;
            plt.scatter(dates[i], close_val[i], c="r")

    print "Ending with $", capital

    # Show the plot
    plt.show()

# Function to convert bollinger bands to %b
def bbp(price):
    up, mid, low = BBANDS(close, timeperiod=20, nbdevup=2, nbdevdn=2, matype=0)
    bbp = (price['close'] - low) / (up - low)
    return bbp

# Uses the rsi and %b method and plots it
def rsi_and_bollinger_plot():

    # Get the closing values
    close = price['close'].values

    # Find the RSI and %b
    rsi = RSI(close, timeperiod=14)
    up, mid, low = BBANDS(close, timeperiod=20, nbdevup=2, nbdevdn=2, matype=0)
    bbp = bbp(price).values

    # Make arrays to hold stuff
    buy = [False] * len(rsi)
    sell = [False] * len(rsi)
    short = [False] * len(rsi)
    short_back = [False] * len(rsi)

    # Print out all good buy and sell points
    bbp_upper = 0.8
    bbp_lower = 0.2
    rsi_upper = 60
    rsi_lower = 40
    for i in range(len(rsi)):
        if(bbp[i] < bbp_upper and rsi[i] < rsi_upper):
            short_back[i]=True
        elif(bbp[i] > bbp_upper and rsi[i] > rsi_upper):
            short[i] = True
        if(bbp[i] > bbp_lower and rsi[i] > rsi_lower):
            sell[i]=True
        elif(bbp[i] < bbp_lower and rsi[i] < rsi_lower):
            buy[i]=True

    # Show it all on a graph
    fig, (reg_plot, rsi_plot, bands_plot) = plt.subplots(3, 1, sharex=True, figsize=(12,8))

    # Debugging stuff
    print "plotting on ", type(dates[0])
    #print close
    #print up
    #print low
    #print rsi
    #print bbp

    # Plot the Adjusted Closing Price with Bollinger Bands
    reg_plot.plot(dates, close)
    reg_plot.plot(dates, up)
    reg_plot.plot(dates, low)
    reg_plot.set_ylabel("Adjusted Closing Price")
    reg_plot.set_xlabel("Date")

    # Plot the RSI
    rsi_plot.plot(dates, rsi)
    rsi_plot.set_ylabel("RSI")
    rsi_plot.set_xlabel("Date")
    rsi_plot.fill_between(np.array(dates), y1=rsi_lower, y2=rsi_upper, color='#ffac33', alpha='0.25')

    # Plot the %Bollinger Stuff
    bands_plot.plot(dates, bbp)
    bands_plot.set_ylabel("%b")
    bands_plot.set_xlabel("Date")
    bands_plot.fill_between(np.array(dates), y1=bbp_lower, y2=bbp_upper, color='#ffac33', alpha='0.25')

    # Plot the Buy/Sell points
    bought_at = 0
    short_holding = False
    holding = False
    profits = 0
    holding_days = 0.0;
    times_bought = 0.0;
    print dates[0]
    print dates[5]

    # Plot all the buy and sell points
    for i in range(len(buy)):
        if holding or short_holding:
            holding_days += 1
        if buy[i] and not holding and not short_holding:
            print("Bought at", dates[i], "for", close[i])
            times_bought += 1
            bought_at = i
            holding = True
            reg_plot.scatter(dates[i], close[i], c="g")
        elif sell[i] and holding and close[bought_at] < close[i] and not short_holding:
            print("Sold at", dates[i], "for", close[i])
            profits += (close[i] - close[bought_at])
            holding = False
            reg_plot.scatter(dates[i], close[i], c="r")
        elif short[i] and not holding and not short_holding:
            print("Shorted at ",dates[i]," for ",close[i])
            times_bought += 1
            bought_at = i
            short_holding = True
            reg_plot.scatter(dates[i], close[i], c="r")
        elif short_back and short_holding and not holding and close[bought_at] > close[i]:
            print("Bought short back at ",dates[i]," for ",close[i])
            profits += (close[bought_at] - close[i])
            short_holding = False
            reg_plot.scatter(dates[i], close[i], c="g")

    print("=== Total profits: $", profits, "===")
    if not times_bought == 0:
        print(" Average holding time:", holding_days/times_bought)

    # Show the plot
    plt.show()

#rsi_and_bollinger_plot()
candlestick_plot()
