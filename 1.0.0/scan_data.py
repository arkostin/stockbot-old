import pandas_datareader.data as web
import pandas as pd
import numpy as np
import json

# Function to get the entire day's data
def get_full_data(symbol=''):
    return 'foo'

# Function to get the last update's data only
def get_data(symbol=''):
    return 'foo'

# Initialize variable
symbol = 'aapl'
last_data = None

# Loop to scan, format, and write data to file
while True:
    data = pd.read_json("https://api.iextrading.com/1.0/tops/last?symbols=AAPL")
    data = data.to_json()
    file = open('data/stock_data_'+symbol, 'a')

    if last_data is None or not data == last_data:
        file.write(data)
        last_data = data

    file.close()
